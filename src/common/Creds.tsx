export default interface Creds {
  login: string;
  password: string;
}
